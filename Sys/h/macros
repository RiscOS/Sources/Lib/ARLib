/* Copyright 1994 Uniqueway Ltd / Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* > h.macros

 * Various useful defines and typedefs that all projects need.
 * � SJ Middleton, 1990
 *

 */

#ifndef __macros_h
# define __macros_h

#ifndef NULL
# define NULL   ((void *) 0)
#endif

#ifndef BOOL
# define BOOL   int
# define TRUE   1
# define FALSE  0
#endif

#ifndef __size_t
# define __size_t 1
typedef unsigned int size_t;   /* from <stddef.h> */
#endif


#define skip()
#define UNUSED(a)   a = a

#define lowest(a,b)     ((a) < (b) ? (a) : (b))
#define highest(a,b)    ((a) > (b) ? (a) : (b))

#define setnull(a)      memset(&a, 0, sizeof(a))

#define cond_set_bit(a,b,c)     if (a) (b) |= (c); else (b) &= ~(c)

#define SWAP(type,x,y)  do { type t = (x); (x) = (y); (y) = t; } while (FALSE);

#define sizeoff(type, member) sizeof(((___type type *)0)->member)

#define limit(val,low,high)     \
    do                          \
    {                           \
        if (val < low)          \
            val = low;          \
        if (val > high)         \
            val = high;         \
    }                           \
    while (FALSE);

#define SHIFT(val,shift) (((shift) > 0) ? ((val) << (shift)) : ((val) >> (-(shift))))

#endif

/* eof h.macros */

