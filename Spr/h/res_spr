/* Copyright 1994 Uniqueway Ltd / Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* > res_spr.h

 * ��SJ Middleton, 1993

 * Handle sprite resources for an application (replaces resspr.c)

 */

#ifndef __res_spr_h
# define __res_spr_h

#ifndef __spr_h
# include "ARLib/spr.h"
#endif

/* -------------------------------------------------------------------------------
 * Description: Load in <App$Dir>.SpritesXX
 * Notes:       res_init mist hav been called first.
 */

extern void resspr_Init(void);

/* -------------------------------------------------------------------------------
 * Description: Merge a sprite file into the resource file.
 * Parameters:  const char *filename -- full pathname of a sprite file/
 * Returns:     success
 * Notes:       Beware - this may result in the sprite area moving.
 */

extern BOOL resspr_AddSprites(const char *filename);

/* -------------------------------------------------------------------------------
 * Description: Get anchor for sprite resources.
 */

extern spr_area resspr_Anchor(void);

/* -------------------------------------------------------------------------------
 * Description: Get sprite_area pointer for sprite resources.
 */

extern sprite_area *resspr_Area(void);

#endif

/* end of res_spr.h */
