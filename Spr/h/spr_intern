/* Copyright 1994 Uniqueway Ltd / Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* > spr_intern.h

 * � SJ Middleton, 1993

 */

#ifndef __spr_intern_h
# define __spr_intern_h

#ifndef __spr_h
# include "ARLib/spr.h"
#endif

struct spr__area
{
    sprite_area *area;      /* must be first for assembler */
    mem_allocfn alloc;
    mem_freefn  free;
    const char  *name;
};

#define MAX_PALETTE_SIZE        (256*2*4)
#define err_InvalidSpriteMode   0x708

extern os_error *sprop3(int reason, spr_area anchor, void *ptr);
extern os_error *sprop4(int reason, spr_area anchor, void *ptr, const char *r4);
extern os_error *sprop4i(int reason, spr_area anchor, void *ptr, int r4);
extern os_error *spr_AnchorIsNull(void);
extern os_error *spr_OutOfMemory(void);
extern os_error *spr_CheckAnchor(spr_area anchor);
extern BOOL extendto(spr_area anchor, int size, os_error **e);
extern BOOL extendby(spr_area anchor, int by, os_error **e);

/* spr_scale shared bits */

#define I_VAL_SHIFT 10
#define I_VAL       (1 << I_VAL_SHIFT)
#define I_XVAL      I_VAL
#define I_YVAL      I_VAL
#define AREA_SHIFT  (2*I_VAL_SHIFT)
#define AREA_ROUND  (1 << (AREA_SHIFT-1))

#define fBits AREA_SHIFT
#define fRnd (1 << (fBits-1))
#define fMul (1 << fBits)
#define fTop (255 * fMul)
#define fFree (1 << (31-8-fBits))

#endif

/* eof spr_intern.h */
