# Copyright 2000 Pace Micro Technology plc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for ARLib libraries
#

COMPONENT       = ARLib
LIBRARY         = ARIntLib
LIBRARIES       = ${LIBRARY}
EXPORTS         = other_arlib_hdrs
CDEFINES        = -DNDEBUG
CFLAGS          = -zo
VPATH           = DBox Draw File Mem Spr Struc Sys Wimp
OBJS            = dbox dboxes2 dboxinc dboxquery popup saveas tasksave tasksum \
                  file file2 fileasm filecanon filer fileract filescan opt \
                  c_alloc d_alloc flex f_alloc heap h_alloc m_alloc \
                  res_spr spr spr_disp spr_icons sprasm \
                  array \
                  alarm baricon clipboard coords device event fileicon help iconmenu icons \
                  menu msgclaim pane poll plotsprite redraw taskwindow template wimp \
                  wimpasm wimpt win wintitle wmisc wmiscclose wmiscflush wmiscquit \
                  wmiscshift xferrecv xfersend \
                  akbd msgs os res screenmode strfn sys werr bbcasm visdelasm
SOURCES_TO_SYMLINK = ${wildcard DBox/c/*} ${wildcard DBox/h/*} \
                     ${wildcard Draw/h/*} \
                     ${wildcard File/c/*} ${wildcard File/h/*} ${wildcard File/s/*} \
                     ${wildcard Mem/c/*} ${wildcard Mem/h/*} \
                     ${wildcard Spr/c/*} ${wildcard Spr/h/*} ${wildcard Spr/s/*} \
                     ${wildcard Struc/c/*} ${wildcard Struc/h/*} \
                     ${wildcard Sys/c/*} ${wildcard Sys/h/*} ${wildcard Sys/s/*} \
                     ${wildcard Wimp/c/*} ${wildcard Wimp/h/*} ${wildcard Wimp/s/*}

include CLibrary

other_arlib_hdrs:
	${MAKE} -C DBox  export PHASE=hdrs
	${MAKE} -C Draw  export PHASE=hdrs
	${MAKE} -C File  export PHASE=hdrs
	${MAKE} -C Mem   export PHASE=hdrs
	${MAKE} -C Spr   export PHASE=hdrs
	${MAKE} -C Struc export PHASE=hdrs
	${MAKE} -C Sys   export PHASE=hdrs
	${MAKE} -C Wimp  export PHASE=hdrs

clean::
	@${MAKE} -C DBox clean
	${STRIPDEPEND}
	@${MAKE} -C Draw clean
	${STRIPDEPEND}
	@${MAKE} -C File clean
	${STRIPDEPEND}
	@${MAKE} -C Mem clean
	${STRIPDEPEND}
	@${MAKE} -C Spr clean
	${STRIPDEPEND}
	@${MAKE} -C Struc clean
	${STRIPDEPEND}
	@${MAKE} -C Sys clean
	${STRIPDEPEND}
	@${MAKE} -C Wimp clean
	${STRIPDEPEND}

# Dynamic dependencies:
